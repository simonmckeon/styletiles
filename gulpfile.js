var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var del = require('del');

var	paths = {
	scss_entry: './src/styles/styles.scss',
	jade_entry: './src/index.jade',
	dest_dir: './dest'
}

gulp.task('jade', function () {
	return gulp.src(paths.jade_entry)
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest(paths.dest_dir));
});

gulp.task('sass', function () {
	return gulp.src(paths.scss_entry)
		.pipe(sass())
		.pipe(gulp.dest(paths.dest_dir));
});

gulp.task('clean', function () {
	del([paths.dest_dir]);
});

gulp.task('default', ['clean', 'jade', 'sass']);